package com.instanceof42.maptools;

import org.junit.Ignore;
import org.junit.Test;

/**
 * Tests for @link{{@link ImageProcessor}}
 */
public class ImageProcessorTest {

    @Ignore
    @Test
    public void all() {

        ImageProcessor.loadImage("D:/Dracandria.png")
                .slice(256, 256)
                .writeTiles("D:/dracandria-map/tiles", 5)
                .scale(50, 50)
                .writeImage("D:/Dracandria_50.png")
                .slice(256, 256)
                .writeTiles("D:/dracandria-map/tiles", 4)
                .scale(50, 50)
                .slice(256, 256)
                .writeTiles("D:/dracandria-map/tiles", 3)
                .scale(50, 50)
                .slice(256, 256)
                .writeTiles("D:/dracandria-map/tiles", 2)
                .scale(50, 50)
                .slice(256, 256)
                .writeTiles("D:/dracandria-map/tiles", 1);
    }

}