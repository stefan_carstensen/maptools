package com.instanceof42.maptools;

import java.awt.image.BufferedImage;
import java.util.Map;

import static org.junit.Assert.*;

public class ImageSlicerTest {

    @org.junit.Test
    public void slice() {

        BufferedImage image = new BufferedImage(1050, 550, BufferedImage.TYPE_INT_RGB);


        ImageSlicer slicer = new ImageSlicer();
        Map<Coordinate, BufferedImage> imageMap = slicer.slice(image, 256, 256);

        assertEquals(15, imageMap.size());
        assertNotNull(imageMap.get(new Coordinate(3,1)));
        assertNull(imageMap.get(new Coordinate(5,1)));
        assertNull(imageMap.get(new Coordinate(3,3)));
    }
}