package com.instanceof42.maptools;

import org.junit.Test;

import java.awt.image.BufferedImage;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Test for {@link ImageScaler}
 */
public class ImageScalerTest {

    @Test
    public void scale() {

        BufferedImage image = new BufferedImage(400, 200, BufferedImage.TYPE_INT_RGB);

        ImageScaler scaler = new ImageScaler();

        BufferedImage result = scaler.scale(image, 50, 50);

        assertNotNull(result);
        assertEquals(200, result.getWidth());
        assertEquals(100, result.getHeight());
        assertEquals(BufferedImage.TYPE_INT_RGB, result.getType());

    }
}