package com.instanceof42.maptools;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.awt.*;
import java.awt.image.BufferedImage;

public class ImageScaler {

    private static final Logger LOG = LogManager.getLogger(ImageScaler.class);

    public BufferedImage scale(BufferedImage inputImage, int scalex, int scaley) {

        BufferedImage bi, bi2;
        Graphics2D gfx;

        int inputWidth = inputImage.getWidth(null);
        int inputHeight = inputImage.getHeight(null);

        Double outputWidthExact = inputWidth * (scalex / 100.0);
        Double outputHeightExact = inputHeight * (scaley / 100.0);

        int outputWidth = outputWidthExact.intValue();
        int outputHeight = outputHeightExact.intValue();

        LOG.debug("scape image from {} * {} to {} * {}", inputWidth, inputHeight, outputWidth, outputHeight);

        bi = new BufferedImage(outputWidth, outputHeight, inputImage.getType());

        gfx = bi.createGraphics();
        gfx.drawImage(inputImage, 0, 0, outputWidth, outputHeight, 0, 0, inputWidth, inputHeight, null);

        bi2 = new BufferedImage(outputWidth, outputHeight, inputImage.getType());
        gfx = bi2.createGraphics();

        gfx.drawImage(bi, 0, 0, outputWidth, outputHeight, null);

        return bi2;

    }

}
