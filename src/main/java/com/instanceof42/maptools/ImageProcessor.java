package com.instanceof42.maptools;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Map;

/**
 * <p>
 * Processes an Image to use it als Tiles for Google Maps.<br/>
 * See <a href="https://developers.google.com/maps/documentation/javascript/groundoverlays">Groundoverlays</a>.
 * </p>
 */
public class ImageProcessor {

    private static final Logger LOG = LogManager.getLogger(ImageProcessor.class);

    ImageScaler scaler;
    ImageSlicer slicer;

    private BufferedImage image;
    private Map<Coordinate, BufferedImage> tiles;


    private ImageProcessor() {
        this.scaler = new ImageScaler();
        this.slicer = new ImageSlicer();
    }

    private ImageProcessor(BufferedImage image) {
        this();
        this.image = image;
    }

    public static ImageProcessor loadImage(String path) {

        try {
            return new ImageProcessor(ImageIO.read(new File(path)));
        } catch (IOException e) {
            LOG.error("failed to read image {}.", path);
        }

        return new ImageProcessor();
    }

    /**
     * Scales the image.
     *
     * @param scaleX the desired width scale in percent.
     * @param scaleY the desired height scale in percent.
     * @return this processor with the scaled image as property;
     */
    public ImageProcessor scale(int scaleX, int scaleY) {

        if (this.image != null) {
            this.image = scaler.scale(this.image, scaleX, scaleY);
        }

        return this;

    }

    /**
     * Slices the image in tiles of equal size.<br/>
     * The border on the right and bottom, which does not yield a whole tile, is cut off.
     *
     * @param tileWidth  the tile's width
     * @param tileHeight the tile's height
     * @return this processor
     */
    public ImageProcessor slice(int tileWidth, int tileHeight) {

        if (this.image != null) {
            this.tiles = slicer.slice(this.image, tileWidth, tileHeight);
        }

        return this;
    }


    /**
     * Writes the image to disk
     *
     * @param path the path (including filename) to write to
     * @return this processor
     */
    public ImageProcessor writeImage(String path) {

        if (this.image != null) {
            try {
                ImageIO.write(image, "png", new File(path));
            } catch (IOException e) {
                LOG.error("failed to write image {}", path);
            }
        }

        return this;
    }

    public ImageProcessor writeTiles(String path, int zoom) {

        if (this.tiles != null) {

            for (Coordinate coordinate : this.tiles.keySet()) {

                String filename = String.format("tile_%s_%s_%s.png", zoom, coordinate.getX(), coordinate.getY());
                LOG.debug("write tile {}.", filename);

                try {
                    ImageIO.write(this.tiles.get(coordinate), "png", new File(path + "/" + filename));
                } catch (IOException e) {
                    LOG.error("failed to write image {}", filename, e);
                }

            }
        }

        return this;

    }


}
