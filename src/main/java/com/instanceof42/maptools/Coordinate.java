package com.instanceof42.maptools;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * The coordinates of a tile.
 */
@Data
@AllArgsConstructor
public class Coordinate {
    private int x;
    private int y;
}
