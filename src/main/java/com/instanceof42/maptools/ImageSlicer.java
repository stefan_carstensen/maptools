package com.instanceof42.maptools;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.awt.image.BufferedImage;
import java.util.HashMap;
import java.util.Map;

public class ImageSlicer {

    private static final Logger LOG = LogManager.getLogger(ImageSlicer.class);


    public Map<Coordinate, BufferedImage> slice(BufferedImage image, int smallWidth, int smallHeight) {

        LOG.debug("image size is {} * {}.", image.getWidth(), image.getHeight());

        int columns = (int)Math.ceil(image.getWidth() / (double)smallWidth);
        int rows = (int)Math.ceil(image.getHeight() / (double)smallHeight);

        LOG.debug("creating {} rows and {} columns.", rows, columns);

        Map<Coordinate, BufferedImage> smallImages = new HashMap<Coordinate, BufferedImage>();


        for (int x = 0; x < columns; x++) {
            for (int y = 0; y < rows; y++) {
                int offsetWidth = smallWidth;
                if (x * smallWidth + smallWidth > image.getWidth()) {
                    offsetWidth = image.getWidth() - x * smallWidth;
                    LOG.debug("clipping width to {}.", offsetWidth);
                }

                int offsetHeight = smallHeight;
                if (y * smallHeight + smallHeight > image.getHeight()) {
                    offsetHeight = image.getHeight() - y * smallHeight;
                    LOG.debug("clipping height to {}.", offsetHeight);
                }

                BufferedImage bufferedImage = new BufferedImage(smallWidth, smallHeight, image.getType());
                bufferedImage.getGraphics().drawImage(image.getSubimage(x * smallWidth, y * smallHeight, offsetWidth, offsetHeight), 0,0, null);

                smallImages.put(new Coordinate(x, y), bufferedImage);
            }
        }

        return smallImages;

    }

}
